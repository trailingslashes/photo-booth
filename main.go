package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// func handlerFunc(w http.ResponseWriter, r *http.Request) {
// 	w.Header().Set("Content-Type", "text/html")
// 	// fmt.Fprintln(w, r.URL.Path) prints the path of the content at the top of the page
// 	if r.URL.Path == "/" {
// 		fmt.Fprint(w, "<h1>Welcome to my site</h1>")
// 	} else if r.URL.Path == "/contact" {
// 		fmt.Fprint(w, "To get in touch, please send an email to <a href=\"mailto:support@photobooth.com\">suport@photobooth.com</a>.")
// 	} else {
// 		// It's better to use the variables for readability. Don't use numbers as status code.
// 		w.WriteHeader(http.StatusNotFound)
// 		fmt.Fprintln(w, "<h1>Error 404!</h1>")
// 	}
// 	// } else {
// 	// 	fmt.Fprintln(w, "<h1>Error 404!</h1>")
// 	// }
// }

func home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "<h1>Welcome to my site</h1>")
}

func contact(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "To get in touch, please send an email to <a href=\"mailto:support@photobooth.com\">suport@photobooth.com</a>.")
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/home", home)
	r.HandleFunc("/contact", contact)
	http.ListenAndServe(":3000", r)
}
